require("dotenv").config();
const express = require("express");
const app = express();
//const port = 3000;

const userRouter = require('./api/users/user.router.js');
app.use(express.json());
app.use('/api/users',userRouter);

/*app.get("/api", (req,res) =>{
    res.json({
        success: 1,
        message: "this is rest api working"
    });
});*/


app.listen(process.env.APP_PORT, () =>{
    console.log("server is running on PORT", process.env.APP_PORT);
});